Release Notes - FTS - Version fts-rest 3.2.33
=============================================

## Improvement sub-task
* [[FTS-207]](https://its.cern.ch/jira/browse/FTS-207) - Improve performance of job listing
* [[FTS-214]](https://its.cern.ch/jira/browse/FTS-214) - Select the best replica at submission
* [[FTS-215]](https://its.cern.ch/jira/browse/FTS-215) - Forbid combined multireplica jobs

## Bug
* [[FTS-208]](https://its.cern.ch/jira/browse/FTS-208) - Reduce memory consumption
* [[FTS-209]](https://its.cern.ch/jira/browse/FTS-209) - Avoid updates of t_optimize_active on submission
