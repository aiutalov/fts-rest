Release Notes - FTS - Version fts-rest 3.2.32
=============================================

## Improvement sub-task
* [[FTS-180]](https://its.cern.ch/jira/browse/FTS-180) - Allow to list banned DNs and SEs
* [[FTS-181]](https://its.cern.ch/jira/browse/FTS-181) - Add API to enable/disable debugging for se/pair
* [[FTS-182]](https://its.cern.ch/jira/browse/FTS-182) - Add API support for configuration via REST

## Bug
* [[FTS-188]](https://its.cern.ch/jira/browse/FTS-188) - On submission, update tables in the same order as FTS3
* [[FTS-197]](https://its.cern.ch/jira/browse/FTS-197) - Avoid to return info for user info in /cs/access_request/{service}
* [[FTS-200]](https://its.cern.ch/jira/browse/FTS-200) - Do not return all information about the cloud storage when checking if the user is registered for it

## Epic
* [[FTS-179]](https://its.cern.ch/jira/browse/FTS-179) - Allow configuration via REST

## Improvement
* [[FTS-185]](https://its.cern.ch/jira/browse/FTS-185) - Add time_window parameter to job listing
* [[FTS-205]](https://its.cern.ch/jira/browse/FTS-205) - Allow specifying the priority on submission time

## New Feature
* [[FTS-184]](https://its.cern.ch/jira/browse/FTS-184) - Authorize a set of DNs for configuration without VO extensions
